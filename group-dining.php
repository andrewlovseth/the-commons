<?php

/*

	Template Name: Group Dining

*/

get_header(); ?>

	<section class="standard">
		<div class="wrapper">

			<div class="section-header">
				<h1><?php the_title(); ?></h1>
			</div>

			<div class="gallery">
				<?php $images = get_field('gallery'); if( $images ): ?>

					<?php foreach( $images as $image ): ?>
						<div class="image">
							<div class="content">
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>			
						</div>
					<?php endforeach; ?>

				<?php endif; ?>
			</div>

			<div class="group-dining-description">
				<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>

			<div class="links">
				<div class="form-link">
					<?php 
					$link = get_field('form_link');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>

					    <div class="btn">
					    	<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
					    </div>					    
					<?php endif; ?>
				</div>

				<div class="menu-link">
					<?php 
					$link = get_field('menu_link');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>

					    <div class="btn">
					    	<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
					    </div>					    
					<?php endif; ?>
				</div>
			</div>

		</div>
	</section>

<?php get_footer(); ?>